// import logo from "./logo.svg";
import "./App.css";
// import BackGroundImg from "./RenderLayout/BackGroundImg";
import RenderLayout from "./RenderLayout/RenderLayout";
// import ThuKinh from "./ThuKinh/ThuKinh";

function App() {
    return (
        <div className="App">
            <RenderLayout />
        </div>
    );
}

export default App;
